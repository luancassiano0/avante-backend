import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'classrooms'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table
        .integer('head_office_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('head_offices')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table
        .integer('teacher_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('teachers')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.string('class_name').notNullable()
      table.timestamp('start_at')
      table.timestamp('finish_at')
      table.timestamp('created_at')
      table.timestamp('updated_at')
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
