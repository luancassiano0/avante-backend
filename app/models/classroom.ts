import { DateTime } from 'luxon'
import { BaseModel, belongsTo, column, hasMany } from '@adonisjs/lucid/orm'
import HeadOffice from './head_office.js'
import type { BelongsTo, HasMany } from '@adonisjs/lucid/types/relations'
import Athlete from './athlete.js'
import Teacher from './teacher.js'

export default class Classroom extends BaseModel {
  @column({ isPrimary: true })
  declare id: number

  @column()
  declare className: string

  @column()
  declare startAt: DateTime

  @column()
  declare finishAt: DateTime

  @column()
  declare headOfficeId: number

  @column()
  declare teacherId: number

  @belongsTo(() => HeadOffice)
  declare headOffice: BelongsTo<typeof HeadOffice>

  @belongsTo(() => Teacher)
  declare teacher: BelongsTo<typeof Teacher>

  @hasMany(() => Athlete)
  declare athletes: HasMany<typeof Athlete>

  @column.dateTime({ autoCreate: true, serializeAs: null })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true, serializeAs: null })
  declare updatedAt: DateTime
}
