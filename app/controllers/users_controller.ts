import User from '#models/user'
import type { HttpContext } from '@adonisjs/core/http'

export default class UsersController {
  async index({ response }: HttpContext) {
    try {
      const payload = await User.all()

      return response.json({
        status: 'ok',
        result: payload,
      })
    } catch (error) {
      return error.message
    }
  }

  async store({ request, response }: HttpContext) {
    try {
      const data = request.only(['username', 'email', 'password'])

      const payload = await User.create(data)

      return response.json({
        status: 'ok',
        result: payload,
      })
    } catch (error) {
      return error.message
    }
  }

  async update({ params, request, response }: HttpContext) {
    try {
      const user = await User.findOrFail(params.id)

      const data = request.only(['username', 'email', 'password'])

      user.merge(data)

      await user.save()

      return response.json({
        status: 'ok',
        result: user,
      })
    } catch (error) {
      return error.message
    }
  }
}
