import User from '#models/user'
import type { HttpContext } from '@adonisjs/core/http'

export default class AuthenticationController {
  async store({ request, response }: HttpContext) {
    try {
      const { email, password } = request.only(['email', 'password'])

      const user = await User.verifyCredentials(email, password)
      const token = await User.accessTokens.create(user)

      return response.json({
        status: 'ok',
        result: user,
        token: token.value!.release(),
      })
    } catch (error) {
      return error.message
    }
  }
}
