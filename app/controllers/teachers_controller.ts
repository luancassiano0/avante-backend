import Teacher from '#models/teacher'
import type { HttpContext } from '@adonisjs/core/http'

export default class TeachersController {
  async index({ response }: HttpContext) {
    try {
      const payload = await Teacher.all()

      return response.json({
        status: 'ok',
        result: payload,
      })
    } catch (error) {
      return error.message
    }
  }

  async store({ request, response }: HttpContext) {
    try {
      const { userId } = request.all()

      const data = request.only(['fullname', 'cref', 'phoneNumber'])

      const payload = await Teacher.create({
        ...data,
        userId,
      })

      return response.json({
        status: 'ok',
        result: payload,
      })
    } catch (error) {
      return error.message
    }
  }

  async show({ params, response }: HttpContext) {
    try {
      const data = await Teacher.findOrFail(params.id)

      return response.json({
        status: 'ok',
        result: data,
      })
    } catch (error) {
      return error.message
    }
  }

  async update({ params, request, response }: HttpContext) {
    try {
      const { userId } = request.all()

      const teacher = await Teacher.findOrFail(params.id)

      const data = request.only(['fullname', 'cref', 'phoneNumber'])

      teacher.merge({
        ...data,
        userId,
      })

      await teacher.save()

      return response.json({
        status: 'ok',
        result: teacher,
      })
    } catch (error) {
      return error.message
    }
  }

  async destroy({ params, response }: HttpContext) {
    try {
      const teacher = await Teacher.findOrFail(params.id)

      teacher.delete()

      return response.json({
        status: 'ok',
      })
    } catch (error) {
      return error.message
    }
  }
}
