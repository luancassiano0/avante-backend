import HeadOffice from '#models/head_office'
import type { HttpContext } from '@adonisjs/core/http'

export default class HeadOfficesController {
  async index({ response }: HttpContext) {
    try {
      const data = await HeadOffice.all()

      return response.json({
        status: 'ok',
        result: data,
      })
    } catch (error) {
      return error.message
    }
  }

  async store({ request, response }: HttpContext) {
    try {
      const data = request.only([
        'name',
        'address',
        'addressNumber',
        'postalCode',
        'neighborhood',
        'city',
        'state',
      ])

      const payload = await HeadOffice.create(data)

      return response.json({
        status: 'ok',
        result: payload,
      })
    } catch (error) {
      return error.message
    }
  }

  async show({ params, response }: HttpContext) {
    try {
      const data = await HeadOffice.findOrFail(params.id)

      return response.json({
        status: 'ok',
        result: data,
      })
    } catch (error) {
      return error.message
    }
  }

  async update({ params, request, response }: HttpContext) {
    try {
      const headOffice = await HeadOffice.findOrFail(params.id)

      const data = request.only([
        'name',
        'address',
        'addressNumber',
        'postalCode',
        'neighborhood',
        'city',
        'state',
      ])

      headOffice.merge(data)

      await headOffice.save()

      return response.json({
        status: 'ok',
        result: headOffice,
      })
    } catch (error) {
      return error.message
    }
  }

  async destroy({ params, response }: HttpContext) {
    try {
      const headOffice = await HeadOffice.findOrFail(params.id)

      headOffice.delete()

      return response.json({
        status: 'ok',
      })
    } catch (error) {
      return error.message
    }
  }
}
