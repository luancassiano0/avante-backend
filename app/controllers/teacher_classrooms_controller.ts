import Teacher from '#models/teacher'
import type { HttpContext } from '@adonisjs/core/http'

export default class TeacherClassroomsController {
  async show({ params, response }: HttpContext) {
    try {
      const teacher = await Teacher.findOrFail(params.id)

      await teacher.load('classrooms', (query) =>
        query.select('id', 'className', 'startAt', 'finishAt')
      )

      return response.json({
        status: 'ok',
        result: teacher,
      })
    } catch (error) {
      return error.message
    }
  }
}
