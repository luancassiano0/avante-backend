import Classroom from '#models/classroom'
import HeadOffice from '#models/head_office'
import type { HttpContext } from '@adonisjs/core/http'

export default class HeadOfficeClassroomsController {
  async index({ request, response }: HttpContext) {
    try {
      const { classroomId } = request.all()

      const classRoom = await Classroom.findOrFail(classroomId)

      await classRoom.load('headOffice', (query) => query.select('name'))

      return response.json({
        status: 'ok',
        result: classRoom,
      })
    } catch (error) {
      return error.message
    }
  }

  async show({ params, response }: HttpContext) {
    try {
      const headOffice = await HeadOffice.findOrFail(params.id)

      await headOffice.load('classrooms', (query) =>
        query.select('className', 'startAt', 'finishAt')
      )

      return response.json({
        status: 'ok',
        result: headOffice,
      })
    } catch (error) {
      return error.message
    }
  }
}
