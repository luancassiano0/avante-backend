import Athlete from '#models/athlete'
import type { HttpContext } from '@adonisjs/core/http'

export default class AthletesController {
  async index({ response }: HttpContext) {
    try {
      const payload = await Athlete.all()

      return response.json({
        status: 'ok',
        result: payload,
      })
    } catch (error) {
      return error.message
    }
  }

  async store({ request, response }: HttpContext) {
    try {
      const { userId, classroomId } = request.all()

      const data = request.only(['fullname'])

      const payload = await Athlete.create({
        ...data,
        userId,
        classroomId,
      })

      return response.json({
        status: 'ok',
        result: payload,
      })
    } catch (error) {
      return error.message
    }
  }

  async show({ params, response }: HttpContext) {
    try {
      const data = await Athlete.findOrFail(params.id)

      return response.json({
        status: 'ok',
        result: data,
      })
    } catch (error) {
      return error.message
    }
  }

  async update({ params, request, response }: HttpContext) {
    try {
      const { userId, classroomId } = request.all()

      const athlete = await Athlete.findOrFail(params.id)

      const data = request.only(['fullname'])

      athlete.merge({
        ...data,
        userId,
        classroomId,
      })

      await athlete.save()

      return response.json({
        status: 'ok',
        result: athlete,
      })
    } catch (error) {
      return error.message
    }
  }

  async destroy({ params, response }: HttpContext) {
    try {
      const athlete = await Athlete.findOrFail(params.id)

      athlete.delete()

      return response.json({
        status: 'ok',
      })
    } catch (error) {
      return error.message
    }
  }
}
