import Classroom from '#models/classroom'
import type { HttpContext } from '@adonisjs/core/http'

export default class ClassroomsController {
  async index({ response }: HttpContext) {
    try {
      const classrooms = await Classroom.all()

      return response.json({
        status: 'ok',
        result: classrooms,
      })
    } catch (error) {
      return error.message
    }
  }

  async store({ request, response }: HttpContext) {
    try {
      const { headOfficeId, teacherId } = request.all()

      const data = request.only(['className', 'startAt', 'finishAt'])

      const payload = await Classroom.create({
        ...data,
        headOfficeId,
        teacherId,
      })

      return response.json({
        status: 'ok',
        result: payload,
      })
    } catch (error) {
      return error.message
    }
  }

  async show({ params, response }: HttpContext) {
    try {
      const data = await Classroom.findOrFail(params.id)

      await data.load('teacher', (query) => query.select('fullname'))

      await data.load('athletes', (query) => query.select('fullname'))

      return response.json({
        status: 'ok',
        result: data,
      })
    } catch (error) {
      return error.message
    }
  }

  async update({ params, request, response }: HttpContext) {
    try {
      const { headOfficeId, teacherId } = request.all()

      const classRoom = await Classroom.findOrFail(params.id)

      const data = request.only(['className', 'startAt', 'finishAt'])

      classRoom.merge({
        ...data,
        headOfficeId,
        teacherId,
      })

      await classRoom.save()

      return response.json({
        status: 'ok',
        result: classRoom,
      })
    } catch (error) {
      return error.message
    }
  }

  async destroy({ params, response }: HttpContext) {
    try {
      const classRoom = await Classroom.findOrFail(params.id)

      classRoom.delete()

      return response.json({
        status: 'ok',
      })
    } catch (error) {
      return error.message
    }
  }
}
