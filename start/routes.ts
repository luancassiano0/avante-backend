const UsersController = () => import('#controllers/users_controller')
const AuthenticationController = () => import('#controllers/authentication_controller')
const HeadOfficesController = () => import('#controllers/head_offices_controller')
const ClassroomsController = () => import('#controllers/classrooms_controller')
const HeadOfficeClassroomsController = () =>
  import('#controllers/head_office_classrooms_controller')
const AthletesController = () => import('#controllers/athletes_controller')
const TeacherController = () => import('#controllers/teachers_controller')
const TeacherClassroomsController = () => import('#controllers/teacher_classrooms_controller')

import router from '@adonisjs/core/services/router'

import { middleware } from './kernel.js'

router.get('/', async () => {
  return {
    hello: 'world',
  }
})

router.resource('/api/user', UsersController).apiOnly()
router.post('/api/auth', [AuthenticationController, 'store'])

router
  .group(() => {
    router.resource('/headOffice', HeadOfficesController).apiOnly()
    router.resource('/class', ClassroomsController).apiOnly()
    router.resource('/classByHeadOffice', HeadOfficeClassroomsController).apiOnly()
    router.resource('/athlete', AthletesController).apiOnly()
    router.resource('/teacher', TeacherController).apiOnly()
    router.resource('/classByTeacher', TeacherClassroomsController).apiOnly()
  })
  .prefix('/api')
  .use(middleware.auth())
